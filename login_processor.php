<?php

// DB creds
include("config.inc.php");

// Create connection
$conn = new mysqli($server, $user, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Grabs form data from previous page
$varUsername = $_POST['formUsername'];
$varPassword = $_POST['formPassword'];

// Converts PW to md5
$hash = md5($varPassword);

// Searches db for match based on user/pass from previous page
$sql = "SELECT id FROM form_fillouts WHERE username = '$varUsername' and password = '$hash'";
$result = $conn->query($sql);

// Counts rows
$count = $result->num_rows;

if ($count  === 1) {
	// Create session
	session_start();
    $_SESSION["username"] = $varUsername;
    $_SESSION["password"] = $hash;
    // Redirects
    header('Refresh: 0;url= success.php');
} else {
    echo "Wrong username or password.";
}

// Close connection

$conn->close($conn);