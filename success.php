<?php

// DB creds
include("config.inc.php");

// Create connection
$conn = new mysqli($server, $user, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Grab session variables from previous page (user / pass)
session_start();
$username = $_SESSION["username"];
$password = $_SESSION["password"];

// Search for user / pass in db
$sql = "SELECT id,firstname,lastname,username FROM form_fillouts WHERE username = '$username' and password = '$password'";
// Run query, store result
$result = $conn->query($sql);
// Fetch row data from result
$row = mysqli_fetch_row($result);

// Fail if result not equal to 1
if ($result->num_rows != 1) {
	echo "Failed for some reason..";
	exit;
}

// Echo results
echo '<div style="height:100px"></div>';
echo '<div align="center" style="font-size:x-large">';
echo 'Thank you, ' . $row[1] . ' ' . $row[2] . '!<br><br>';
echo 'Your ID is: ' . $row[0] . '<br>';
echo 'Your username is: ' . $row[3] . '<br>';
echo 'Your encrypted PW is: ' . $password . '<br><br>';
echo '</div>';

// Close connection
$conn->close($conn);